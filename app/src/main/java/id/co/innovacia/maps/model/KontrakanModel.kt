package id.co.innovacia.maps.model

data class KontrakanModel (
    val ownerEmail: String? = null,
    val ownerNama: String? = null,
    val ownerTelepon: String? = null,
    val alamat: String? = null,
    val luasTanah: Int? = null,
    val luasBangunan: Int? = null,
    val kamarTidur: Int? = null,
    val kamarMandi: Int? = null,
    val garasi: Int? = null,
    val ac: Int? = null,
    val perbulan: Int? = null,
    val pertahun: Int? = null,
    val latitude: Double? = null,
    val longitude: Double? = null,
    val status: Int? = null,
    val foto: MutableList<String>? = null,
    val sold: String? = null
)