package id.co.innovacia.maps

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import id.co.innovacia.maps.helper.MethodHelper
import kotlinx.android.synthetic.main.activity_login.*
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.database.*
import id.co.innovacia.maps.model.UserModel

class Login : AppCompatActivity() {

    private val mMethodHelper = MethodHelper()
    lateinit var mAuth: FirebaseAuth
    lateinit var mDatabase: DatabaseReference

    // Default user = pencari kontrakan
    private var userState = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mAuth = FirebaseAuth.getInstance()

        spinner()

        // handling visibility layout Register
        signup.setOnClickListener {
            signup.startAnimation(MethodHelper.animasi)
            login_lay.visibility = View.GONE
            signup_lay.visibility = View.VISIBLE
            resetView()
        }

        // handling visibility layout Login
        login.setOnClickListener {
            login_lay.visibility = View.VISIBLE
            signup_lay.visibility = View.GONE
            resetView()
        }

        btn_login.setOnClickListener {
            btn_login.startAnimation(MethodHelper.animasi)
            if (validasiSignIn()) {
                signInProses()
            }
        }

        btn_signup.setOnClickListener {
            btn_signup.startAnimation(MethodHelper.animasi)
            if (validasiSignUp()) {
                signUpProses()
            }
        }

        btn_forgot.setOnClickListener {
            btn_forgot.startAnimation(MethodHelper.animasi)

            val i = Intent(this,ForgotPass::class.java)
            startActivity(i)
        }
    }

    private fun spinner(){
        val spinnerData = arrayOf(
            "Saya Mencari Kontrakan",
            "Saya Memiliki Kontrakan")

        val arrayAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, spinnerData)
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerState?.adapter = arrayAdapter

        spinnerState?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) { }
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                if (position == 0) {
                    // Pencari Kontrakan
                    userState = 1
                } else if (position == 1) {
                    // Owner Kontrakan
                    userState = 2
                }
            }
        }
    }

    private fun validasiSignUp(): Boolean{
        var cek = 0
        var warning = ""

        if(signup_user.text.isEmpty()){
            warning = "Isi kolom email"; cek++
        }
        if(signup_pass.text!!.length < 6){
            warning = "Password minimal 6 karakter"; cek++
        }
        if (signup_pass_again.text!!.toString() != signup_pass_again.text.toString()) {
            warning = "Password tidak sesuai"; cek++
        }
        if (signup_nama.text.isEmpty()) {
            warning = "Isi kolom nama"; cek++
        }
        if (signup_alamat.text.isEmpty()) {
            warning = "Isi kolom alamat"; cek++
        }
        if (signup_telepon.text.isEmpty()) {
            warning = "isi kolom telepon"; cek++
        }

        if (cek == 0){
            return true
        } else {
            mMethodHelper.snackBar(root_lay,warning)
        }

        return false
    }

    private fun signUpProses() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val inputEmail = signup_user?.text.toString()
        val inputPass = signup_pass?.text.toString()

        mAuth.createUserWithEmailAndPassword(inputEmail,inputPass).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                dialog.dismiss()
                val user = mAuth.currentUser
                if (user != null) {
                    val email = user.email
                    val nama = signup_nama?.text.toString()
                    val alamat = signup_alamat?.text.toString()
                    val telepon = signup_telepon?.text.toString()

                    // Permodelan data (Lihat di package model, UserModel)
                    val mUserModel = UserModel(email,nama,alamat,telepon,userState)

                    // Insert data user ke firebase database
                    FirebaseDatabase.getInstance().reference.child("users").child(user.uid).setValue(mUserModel)

                    getUserData()
                }
            } else {
                try {
                    throw task.exception!!
                } catch(e: FirebaseAuthUserCollisionException) {
                    mMethodHelper.snackBar(root_lay,"E-mail telah terdaftar")
                } catch(e: Exception) {
                    Log.d("aim", e.toString())
                }
                dialog.dismiss()
            }
        }
    }

    private fun validasiSignIn(): Boolean{
        var cek = 0
        var warning = ""

        if(login_user.text.isEmpty()){
            warning = "Isi kolom username"; cek++
        }
        if(login_pass.text!!.length < 6){
            warning = "Password minimal 6 karakter"; cek++
        }

        if (cek == 0){
            return true
        } else {
            mMethodHelper.snackBar(root_lay,warning)
        }

        return false
    }

    private fun signInProses() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val email = login_user?.text.toString()
        val password = login_pass?.text.toString()

        mAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(this) { task ->
            if (task.isSuccessful) {
                dialog.dismiss()
                getUserData()
            } else {
                try {
                    throw task.exception!!
                } catch(e: FirebaseAuthInvalidCredentialsException) {
                    mMethodHelper.snackBar(root_lay,"Email dan password tidak sesuai")
                } catch(e: FirebaseAuthUserCollisionException) {
                    mMethodHelper.snackBar(root_lay,"E-mail telah terdaftar")
                } catch(e: Exception) {
                    Log.d("aim", e.toString())
                }
                dialog.dismiss()
            }
        }
    }

    private fun getUserData() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val user = mAuth.currentUser
        mDatabase = FirebaseDatabase.getInstance().getReference("users").child(user!!.uid)
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val mUserModel = dataSnapshot.getValue(UserModel::class.java)
                    MethodHelper.userEmail = mUserModel?.email
                    MethodHelper.userNama = mUserModel?.nama
                    MethodHelper.userAlamat = mUserModel?.alamat
                    MethodHelper.userTelepon = mUserModel?.telepon
                    MethodHelper.userState = mUserModel?.state

                    val i = Intent(applicationContext,Home::class.java)
                    startActivity(i)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                mMethodHelper.snackBar(root_lay,"Data tidak tersedia")
            }
        }
        mDatabase.addListenerForSingleValueEvent(postListener)
    }

    private fun resetView() {
        login_user?.setText("")
        login_pass?.setText("")

        signup_user?.setText("")
        signup_pass?.setText("")
        signup_pass_again?.setText("")
        signup_nama?.setText("")
        signup_alamat?.setText("")
        signup_telepon?.setText("")
    }
}
