package id.co.innovacia.maps

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import id.co.innovacia.maps.helper.MethodHelper
import id.co.innovacia.maps.model.KontrakanModel
import kotlinx.android.synthetic.main.activity_kontrakan.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.list_kontrakan.view.*

class Kontrakan : AppCompatActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kontrakan)
        header_name.text = "Daftar Kontrakan"

        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_input.setOnClickListener {
            val i = Intent(this,CreateUpdateKontrakan::class.java)
            startActivity(i)
        }

        if (MethodHelper.userState == 2) {
            // Pemilik Kontrakan - ambil data kontrakan yang dimiliki owner
            fetchForOwner()
        } else {
            // Admin - ambil semua data kontrakan
            fetchForAdmin()
        }


    }

    private fun getFirebaseKey(koordinat: LatLng,mContext:Context) {
        val builder = AlertDialog.Builder(mContext)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog?.show()

        val rootRef = FirebaseDatabase.getInstance().reference
        val ordersRef = rootRef.child("kontrakan")
            .orderByChild("latitude").equalTo(koordinat.latitude)

        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    dialog?.dismiss()
                    val value = dataSnapshot.value.toString()
                    val firebasekey = value.substring(1,7)

                    val i = Intent(mContext, DetailKontrakan::class.java)
                    i.putExtra("key", firebasekey)
                    startActivity(i)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                dialog?.dismiss()
                Log.d("aim", "loadPost:onCancelled", databaseError.toException())
            }
        }
        ordersRef.addListenerForSingleValueEvent(valueEventListener)
    }

    private fun fetchForOwner() {
        // Menampilkan hanya kontrakan milik user tersebut
        val userEmail = MethodHelper.userEmail
        var query = FirebaseDatabase.getInstance().getReference("kontrakan").orderByChild("ownerEmail")
            .equalTo(userEmail)

        val options = FirebaseRecyclerOptions.Builder<KontrakanModel>()
            .setQuery(query, KontrakanModel::class.java)
            .setLifecycleOwner(this)
            .build()

        val adapter = object : FirebaseRecyclerAdapter<KontrakanModel, KontrakanHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KontrakanHolder {
                return KontrakanHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_kontrakan, parent, false))
            }

            override fun onBindViewHolder(holder: KontrakanHolder, position: Int, model: KontrakanModel) {
                holder.bindView(model)

                holder.itemView.setOnClickListener {
                    val koordinat = LatLng(model.latitude!!, model.longitude!!)
                    getFirebaseKey(koordinat,holder.itemView.context)
                }
            }

            override fun onDataChanged() {
                // Update UI
            }
        }

        recyclerview?.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
        recyclerview?.adapter = adapter
    }

    private fun fetchForAdmin() {
        // Menampilkan hanya kontrakan milik user tersebut
        val userEmail = MethodHelper.userEmail
        var query = FirebaseDatabase.getInstance().getReference("kontrakan")

        val options = FirebaseRecyclerOptions.Builder<KontrakanModel>()
            .setQuery(query, KontrakanModel::class.java)
            .setLifecycleOwner(this)
            .build()

        val adapter = object : FirebaseRecyclerAdapter<KontrakanModel, KontrakanHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KontrakanHolder {
                return KontrakanHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_kontrakan, parent, false))
            }

            override fun onBindViewHolder(holder: KontrakanHolder, position: Int, model: KontrakanModel) {
                holder.bindView(model)

                holder.itemView.setOnClickListener {
                    val koordinat = LatLng(model.latitude!!, model.longitude!!)
                    getFirebaseKey(koordinat,holder.itemView.context)
                }
            }

            override fun onDataChanged() {
                // Update UI
            }
        }

        recyclerview?.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
        recyclerview?.adapter = adapter
    }

    class KontrakanHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(mKontrakanModel: KontrakanModel) {

            itemView.user.text = mKontrakanModel.ownerEmail
            itemView.telepon.text = mKontrakanModel.ownerTelepon
            itemView.alamat.text = mKontrakanModel.alamat

            if (mKontrakanModel.status == 0) {
                itemView.status.text = "Belum Tervalidasi"
            } else {
                itemView.status.visibility = View.GONE
            }

            if (mKontrakanModel.sold == "1") {
                itemView.sold.visibility = View.VISIBLE
            } else {
                itemView.sold.visibility = View.GONE
            }

            if (mKontrakanModel.foto != null) {
                Glide.with(itemView.context).load(mKontrakanModel.foto[0]).into(itemView.photo)
            }
        }
    }
}
