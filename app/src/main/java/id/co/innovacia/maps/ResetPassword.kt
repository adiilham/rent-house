package id.co.innovacia.maps

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.firebase.auth.FirebaseAuth
import id.co.innovacia.maps.helper.MethodHelper
import kotlinx.android.synthetic.main.activity_reset_password.*
import kotlinx.android.synthetic.main.header.*

class ResetPassword : AppCompatActivity() {

    private val mMethodHelper = MethodHelper()
    lateinit var mAuth: FirebaseAuth

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reset_password)
        header_name.text = "Reset Password"

        mAuth = FirebaseAuth.getInstance()

        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_ok.setOnClickListener {
            if (old_pass.text.isNotEmpty() && new_pass.text.isNotEmpty() && new_pass_again.text.isNotEmpty()) {
                mAuth.signInWithEmailAndPassword(MethodHelper.userEmail!!,old_pass.text.toString()).addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        if (new_pass.text.toString() == new_pass_again.text.toString()) {
                            val user = mAuth.currentUser
                            user?.updatePassword(new_pass.text.toString())?.addOnCompleteListener { task ->
                                if (task.isSuccessful) {
                                    finish()
                                } else {
                                    mMethodHelper.snackBar(root_lay,"Reset password gagal")
                                }
                            }
                        } else {
                            mMethodHelper.snackBar(root_lay,"Password baru dan konfirmasi tidak cocok")
                        }
                    } else {
                        mMethodHelper.snackBar(root_lay,"Password saat ini salah")
                    }
                }
            } else {
                mMethodHelper.snackBar(root_lay,"Semua kolom harus diisi")
            }
        }
    }
}
