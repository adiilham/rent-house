package id.co.innovacia.maps.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.innovacia.maps.R
import id.co.innovacia.maps.helper.MethodHelper
import id.co.innovacia.maps.Kontrakan
import id.co.innovacia.maps.UpdateProfile
import id.co.innovacia.maps.ResetPassword
import kotlinx.android.synthetic.main.fragment_profile.*
import com.google.firebase.auth.FirebaseAuth
import android.util.Log

class ProfileFragment : Fragment() {

    lateinit var mAuth: FirebaseAuth

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAuth = FirebaseAuth.getInstance()

        generatedView()

        btn_kontrakan?.setOnClickListener {
            btn_kontrakan?.startAnimation(MethodHelper.animasi)

            val i = Intent(activity,Kontrakan::class.java)
            activity?.startActivity(i)
        }

        btn_edit?.setOnClickListener {
            btn_edit?.startAnimation(MethodHelper.animasi)

            val i = Intent(activity,UpdateProfile::class.java)
            activity?.startActivity(i)
        }

        btn_reset?.setOnClickListener {
            btn_reset?.startAnimation(MethodHelper.animasi)

            val i = Intent(activity,ResetPassword::class.java)
            activity?.startActivity(i)
        }

        btn_keluar?.setOnClickListener {
            btn_keluar?.startAnimation(MethodHelper.animasi)
            logOut()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun generatedView() {
        profile_logo?.text = MethodHelper.userNama?.get(0).toString().capitalize()
        profile_name?.text = MethodHelper.userNama?.capitalize()
        profile_telepon?.text = MethodHelper.userTelepon

        if (MethodHelper.userState == 1) {
            // Pencari kontrakan
            profile_status?.text = "Pencari Kontrakan"
            btn_kontrakan?.visibility = View.GONE
        } else if (MethodHelper.userState == 2) {
            // Owner kontrakan
            profile_status?.text = "Pemilik Kontrakan"
            btn_kontrakan?.visibility = View.VISIBLE
        } else {
            // Admin
            btn_kontrakan?.visibility = View.VISIBLE
            profile_status?.text = "Administrator"
            tx_kontrakan?.text = "Daftar Kontrakan"
        }
    }

    private fun logOut() {
        mAuth.signOut()

        // Restart Aplikasi
        try {
            val i = activity?.packageManager?.getLaunchIntentForPackage(activity!!.packageName)
            i?.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i?.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(i)
        } catch (e:Exception){
            Log.d("aim",e.toString())
        }
    }
}
