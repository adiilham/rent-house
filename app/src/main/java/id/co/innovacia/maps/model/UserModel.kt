package id.co.innovacia.maps.model

data class UserModel (

    // State 1 = Pencari kontrakan
    // State 2 = Owner kontrakan
    // State 3 = Admin

    val email       : String? = null,
    val nama        : String? = null,
    val alamat      : String? = null,
    val telepon     : String? = null,
    val state       : Int? = null
)