package id.co.innovacia.maps

import android.content.pm.PackageManager
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import android.Manifest
import android.annotation.SuppressLint
import android.content.Intent
import android.location.LocationListener
import android.os.Looper
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.gms.location.*
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.model.*
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import com.livinglifetechway.quickpermissions_kotlin.util.QuickPermissionsOptions
import kotlinx.android.synthetic.main.activity_maps.*
import java.security.AccessController.getContext


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, LocationListener,
    GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private var mMap: GoogleMap? = null
    private lateinit var mLastLocation: Location
    private lateinit var mLocationResult: LocationRequest
    private lateinit var mLocationCallback: LocationCallback
    private var mCurrLocationMarker: Marker? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private lateinit var mLocationRequest: LocationRequest
    private var mFusedLocationClient: FusedLocationProviderClient? = null

    var latLong : LatLng? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        obtieneLocalizacion()

    }

    @SuppressLint("MissingPermission")
    private fun obtieneLocalizacion() = runWithPermissions(Manifest.permission.ACCESS_FINE_LOCATION) {
        val fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        if (mFusedLocationClient == null) {
            Toast.makeText(this,"Turn ON GPS",Toast.LENGTH_LONG).show()
        } else {
            mFusedLocationClient?.lastLocation?.addOnSuccessListener { location: Location? ->
                var latitude = location?.latitude
                var longitude = location?.longitude

                Toast.makeText(this, "LAT : $latitude, LON : $longitude", Toast.LENGTH_LONG).show()
            }
        }
    }

    override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun onProviderEnabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
    override fun onProviderDisabled(provider: String?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.setMapStyle(MapStyleOptions.loadRawResourceStyle(this, R.raw.map_style))
        val cikarang = LatLng(-6.26111,107.15278)

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient()
                mMap!!.isMyLocationEnabled = true
                mMap!!.moveCamera(CameraUpdateFactory.newLatLng(cikarang))
                mMap!!.animateCamera(CameraUpdateFactory.zoomTo(4.1f))
            }
        } else {
            buildGoogleApiClient()
            mMap!!.isMyLocationEnabled = true
            mMap!!.moveCamera(CameraUpdateFactory.newLatLng(cikarang))
            mMap!!.animateCamera(CameraUpdateFactory.zoomTo(4.1F))
        }

        if (intent.hasExtra("latitude")) {
            val latitude = intent.getDoubleExtra("latitude",0.0)
            val longitude = intent.getDoubleExtra("longitude",0.0)
            btn_save.visibility = View.GONE

            val koordinat = LatLng(latitude,longitude)
            val markerOptions = MarkerOptions()
            markerOptions.position(koordinat)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))

            mMap?.clear()
            mMap!!.addMarker(markerOptions)
        }

        mMap?.setOnMapClickListener {
            mMap?.clear()
            latLong = it

            val markerOptions = MarkerOptions()
            markerOptions.position(it)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))

            mMap!!.addMarker(markerOptions)
            Log.d("aim",it.toString())
        }

        btn_save?.setOnClickListener {
            val latitude = latLong?.latitude
            val longitude = latLong?.longitude

            val i = Intent()
            i.putExtra("latitude",latitude)
            i.putExtra("longitude",longitude)
            setResult(RESULT_OK,i)
            finish()
        }

    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .addApi(LocationServices.API).build()
        mGoogleApiClient!!.connect()
    }

    override fun onConnected(bundle: Bundle?) {
        mLocationRequest = LocationRequest()
        mLocationRequest.interval = 1000
        mLocationRequest.fastestInterval = 1000
        mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
//            mFusedLocationClient?.requestLocationUpdates(mLocationRequest,mLocationCallback, Looper.myLooper())

        }
    }

    override fun onLocationChanged(location: Location) {

        mLastLocation = location
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker!!.remove()
        }
        //Place current location marker
        var latLng = LatLng(location.latitude, location.longitude)
        Log.d("aim", "test")
        Log.d("aim","LAT : ${location.latitude} ,LON : ${location.longitude}")


        val markerOptions = MarkerOptions()
        markerOptions.position(latLng)
        markerOptions.title("Current Position")
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
        mCurrLocationMarker = mMap!!.addMarker(markerOptions)

        //move map camera
        mMap!!.moveCamera(CameraUpdateFactory.newLatLng(latLng))
        mMap!!.animateCamera(CameraUpdateFactory.zoomTo(0F))
        Log.d("aim", "$latLng")

        //stop location updates
        if (mGoogleApiClient != null) {
            mFusedLocationClient?.removeLocationUpdates(mLocationCallback)
        }
    }
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Toast.makeText(applicationContext,"connection failed", Toast.LENGTH_SHORT).show()
    }
    override fun onConnectionSuspended(p0: Int) {
        Toast.makeText(applicationContext,"connection suspended", Toast.LENGTH_SHORT).show()
    }
}