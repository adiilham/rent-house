package id.co.innovacia.maps

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_create_update_kontrakan.*
import android.os.Bundle
import android.provider.OpenableColumns
import id.co.innovacia.maps.helper.MethodHelper
import kotlinx.android.synthetic.main.header.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import android.util.Log
import android.view.View
import android.widget.Toast
import id.co.innovacia.maps.model.KontrakanModel
import java.util.*
import android.R.attr.data
import android.annotation.SuppressLint
import android.support.v7.app.AlertDialog
import android.text.Editable
import android.text.TextWatcher
import com.bumptech.glide.Glide
import com.google.firebase.database.*
import java.text.NumberFormat

class CreateUpdateKontrakan : AppCompatActivity() {

    private val mMethodHelper = MethodHelper()
    private var key = mMethodHelper.generatedFirebaseKey()

    private var latitude: Double? = null
    private var longitude: Double? = null
    private var status: Int? = null
    private var sold: String? = null
    private var hargaBulan = 0
    private var hargaTahun = 0
    private var stateGarasi = 0
    private var stateAc = 0

    lateinit var mStorageRef: StorageReference
    lateinit var mDatabase: DatabaseReference

    private val choseImageCode = 2121
    private val chooseLocationCode = 7878

    private var fileNameList = mutableListOf<String>()
    private var fileDoneList = mutableListOf<String>()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_update_kontrakan)
        header_name?.text = "Input Kontrakan"

        // Update data kontrakan
        if (intent.hasExtra("update")) {
            key = intent.getStringExtra("key")
            getDataKontrakan()
            btn_save.text = "Ubah"
        }

        back_link?.setOnClickListener {
            back_link?.startAnimation(MethodHelper.animasi)

            onBackPressed()
        }

        btn_location?.setOnClickListener {
            btn_location?.startAnimation(MethodHelper.animasi)
            val i = Intent(this,MapsActivity::class.java)
            startActivityForResult(i, chooseLocationCode)
        }

        garasi?.setOnCheckedChangeListener { buttonView, isChecked ->
            stateGarasi = if (isChecked) { 1 } else 0
        }

        ac?.setOnCheckedChangeListener { buttonView, isChecked ->
            stateAc = if (isChecked) { 1 } else 0
        }

        harga_bulan.setDelimiter(false)
        harga_bulan.setSpacing(true)
        harga_bulan.setDecimals(false)
        harga_bulan.setSeparator(".")
        harga_bulan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){ }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun afterTextChanged(p0: Editable?){
                hargaBulan = harga_bulan.cleanIntValue
            }
        })

        harga_tahun.setDelimiter(false)
        harga_tahun.setSpacing(true)
        harga_tahun.setDecimals(false)
        harga_tahun.setSeparator(".")
        harga_tahun.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){ }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) { }
            override fun afterTextChanged(p0: Editable?){
                hargaTahun = harga_tahun.cleanIntValue

            }
        })


        btn_upload?.setOnClickListener {
            btn_upload?.startAnimation(MethodHelper.animasi)

            val intent = Intent()
                .setType("image/*")
                .putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
                .setAction(Intent.ACTION_GET_CONTENT)

            startActivityForResult(Intent.createChooser(intent, "Pilih foto"), choseImageCode)
        }

        btn_save?.setOnClickListener {
            btn_save?.startAnimation(MethodHelper.animasi)
            if (validasiInput()) {
                if (btn_save.text == "Ubah") {
                    updateData()
                } else {
                    insertData()
                }
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Action setelah memilih gambar
        if (requestCode == choseImageCode && resultCode == RESULT_OK && data != null) {
            // Upload lebih dari satu foto
            if (data.clipData != null) {
                progress_bar?.visibility = View.VISIBLE
                keterangan_upload?.visibility = View.VISIBLE
                val totalItemsSelected = data.clipData!!.itemCount

                for (i in 0 until totalItemsSelected) {
                    val fileUri = data.clipData!!.getItemAt(i).uri
                    val fileName = getFileName(fileUri)
                    fileNameList.add(fileName)

                    mStorageRef = FirebaseStorage.getInstance().reference.child("images")
                    val fileToUpload = mStorageRef.child(key).child(fileName)

                    var uploadStatus = "Jumlah Foto ${fileDoneList.size}"
                    keterangan_upload?.text = uploadStatus

                    fileToUpload.putFile(fileUri)
                        .addOnSuccessListener { taskSnapshot ->
                            fileToUpload.downloadUrl.addOnSuccessListener { uri ->
                                val downloadUrl = uri.toString()
                                fileDoneList.add(downloadUrl)

                                Log.d("aim","Download url : $downloadUrl")

                                uploadStatus = "Jumlah Foto ${fileDoneList.size}"
                                keterangan_upload?.text = uploadStatus

                                if (fileDoneList.size == fileNameList.size) {
                                    progress_bar?.visibility = View.GONE
                                }
                            }

                        }
                        .addOnFailureListener {
                            fileNameList.remove(fileName)
                            progress_bar?.visibility = View.GONE
                            mMethodHelper.snackBar(root_lay,"Upload gagal - $fileName")
                        }

                }
            }

            // Upload satu foto
            else {
                progress_bar?.visibility = View.VISIBLE
                keterangan_upload?.visibility = View.VISIBLE

                val fileUri = data.data!!
                val fileName = getFileName(fileUri)
                fileNameList.add(fileName)

                mStorageRef = FirebaseStorage.getInstance().reference.child("images")
                val fileToUpload = mStorageRef.child(key).child(fileName)

                var uploadStatus = "Jumlah Foto ${fileDoneList.size}"
                keterangan_upload?.text = uploadStatus

                fileToUpload.putFile(fileUri)
                    .addOnSuccessListener { taskSnapshot ->
                        fileToUpload.downloadUrl.addOnSuccessListener { uri ->
                            val downloadUrl = uri.toString()
                            fileDoneList.add(downloadUrl)

                            progress_bar?.visibility = View.GONE
                            Log.d("aim","Download url : $downloadUrl")

                            uploadStatus = "Jumlah Foto ${fileDoneList.size}"
                            keterangan_upload?.text = uploadStatus
                        }
                    }
                    .addOnFailureListener {
                        fileNameList.remove(fileName)
                        progress_bar?.visibility = View.GONE
                        mMethodHelper.snackBar(root_lay,"Upload gagal - $fileName")
                    }
            }
        }
        else if (requestCode == choseImageCode && resultCode == RESULT_CANCELED){
            mMethodHelper.snackBar(root_lay,"Pilih setidaknya 1 foto")
        }

        // Action setelah memilih koordinat pada peta
        else if (requestCode == chooseLocationCode && data != null) {
            latitude = data.getDoubleExtra("latitude",0.0)
            longitude = data.getDoubleExtra("longitude",0.0)
        }
    }

    private fun getFileName(uri: Uri): String {
        var result: String? = null
        if (uri.scheme == "content") {
            val cursor = contentResolver.query(uri, null, null, null, null)
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))
                }
            } finally {
                cursor!!.close()
            }
        }
        if (result == null) {
            result = uri.path
            val cut = result!!.lastIndexOf('/')
            if (cut != -1) {
                result = result.substring(cut + 1)
            }
        }
        return result
    }

    private fun validasiInput(): Boolean{
        var cek = 0
        var warning = ""

        if(alamat.text.isEmpty()){
            warning = "Isi kolom alamat"; cek++
        }
        if(latitude == 0.0 || longitude == 0.0){
            warning = "Tambahkan titik koordinat pada peta"; cek++
        }
        if(tanah.text.isEmpty()){
            warning = "Isi kolom luas tanah"; cek++
        }
        if (bangunan.text.isEmpty()){
            warning = "Isi kolom luas bangunan"; cek++
        }
        if (kamar.text.isEmpty()) {
            warning = "Isi kolom jumlah kamar tidur"; cek++
        }
        if (wc.text.isEmpty()) {
            warning = "Isi kolom jumlah kamar mandi"; cek++
        }
        if (harga_bulan.text!!.isEmpty()) {
            warning = "Isi kolom harga perbulan"; cek++
        }
        if (harga_tahun.text!!.isEmpty()) {
            warning = "Isi kolom harga pertahun"; cek++
        }
        if (fileDoneList.isEmpty()) {
            warning = "Anda belum memilih foto"; cek++
        }

        if (cek == 0){
            return true
        } else {
            mMethodHelper.snackBar(root_lay,warning)
        }

        return false
    }

    private fun getDataKontrakan() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val mDatabase = FirebaseDatabase.getInstance().getReference("kontrakan").child(key)
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dialog.dismiss()
                if (dataSnapshot.exists()) {
                    val mKontrakanModel = dataSnapshot.getValue(KontrakanModel::class.java)

                    alamat.setText(mKontrakanModel?.alamat)
                    tanah.setText(mKontrakanModel?.luasTanah.toString())
                    bangunan.setText(mKontrakanModel?.luasBangunan.toString())
                    kamar.setText(mKontrakanModel?.kamarTidur.toString())
                    wc.setText(mKontrakanModel?.kamarMandi.toString())
                    latitude = mKontrakanModel?.latitude
                    longitude = mKontrakanModel?.longitude
                    status = mKontrakanModel?.status
                    sold = mKontrakanModel?.sold

                    garasi.isChecked = mKontrakanModel?.garasi == 1
                    ac.isChecked = mKontrakanModel?.ac == 1

                    val perbulan = mKontrakanModel?.perbulan?.let { mMethodHelper.formatRupiah(it) }
                    val pertahun = mKontrakanModel?.pertahun?.let { mMethodHelper.formatRupiah(it) }
                    harga_bulan.setText(perbulan?.replace("Rp ",""))
                    harga_tahun.setText(pertahun?.replace("Rp ",""))

                    val photoArray = mKontrakanModel?.foto
                    if (photoArray != null) {
                        for (i in 0 until photoArray.size) {
//                            mStorageRef = FirebaseStorage.getInstance().reference.child("images").child(photoArray[i])
//                            mStorageRef.delete()

                            fileNameList.add(photoArray[i])
                            fileDoneList.add(photoArray[i])
                        }

                        progress_bar.visibility = View.GONE
                        var uploadStatus = "Jumlah Foto ${fileDoneList.size}"
                        keterangan_upload?.text = uploadStatus
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                dialog.dismiss()
                Log.d("aim", "loadPost:onCancelled", databaseError.toException())
            }
        }
        mDatabase.addListenerForSingleValueEvent(postListener)
    }

    private fun insertData() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        // Status digunakan untuk validasi kontrakan yang diinputkan owner
        // Jika data kontrakan bukan berasal dari admin, maka status = 0 (Data belum Tervalidasi)
        // Jika data kontrakan berasal dari admin, maka status = 1 (Data otomatis Tervalidasi)
        val status = if (MethodHelper.userState == 3) 1 else 0

        val mKontrakanModel = KontrakanModel(
            // Data diambil dari login yang tersimpan di class MethodHelper
            MethodHelper.userEmail,
            MethodHelper.userNama,
            MethodHelper.userTelepon,

            // Data dari layout
            alamat?.text.toString(),
            tanah?.text.toString().toInt(),
            bangunan?.text.toString().toInt(),
            kamar?.text.toString().toInt(),
            wc?.text.toString().toInt(),
            stateGarasi,
            stateAc,
            hargaBulan,
            hargaTahun,
            latitude,
            longitude,
            status,
            fileDoneList,
            if (sold != null) sold else "0"
        )

        // Insert data user ke firebase database
        FirebaseDatabase.getInstance().reference.child("kontrakan").child(key).setValue(mKontrakanModel)
            .addOnCompleteListener {
                finish()
            }.addOnFailureListener {
                mMethodHelper.snackBar(root_lay,"Proses hapus gagal")
                dialog.dismiss()
            }
    }

    private fun updateData() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val headers = HashMap<String,Any?>()
        headers["ownerEmail"] = MethodHelper.userEmail
        headers["ownerNama"] = MethodHelper.userNama
        headers["ownerTelepon"] = MethodHelper.userTelepon
        headers["alamat"] = alamat?.text.toString()
        headers["luasTanah"] = tanah?.text.toString().toInt()
        headers["luasBangunan"] =  bangunan?.text.toString().toInt()
        headers["kamarTidur"] = kamar?.text.toString().toInt()
        headers["kamarMandi"] = wc?.text.toString().toInt()
        headers["garasi"] = stateGarasi
        headers["ac"] = stateAc
        headers["perbulan"] = hargaBulan
        headers["pertahun"] = hargaTahun
        headers["latitude"] = latitude
        headers["longitude"] = longitude
        headers["foto"] = fileDoneList
        headers["sold"] = sold
        headers["status"] = status

        // Update data user ke firebase database
        FirebaseDatabase.getInstance().reference.child("kontrakan").child(key).setValue(headers)
            .addOnCompleteListener {
                finish()
            }
            .addOnCanceledListener {
                dialog.dismiss()
                mMethodHelper.snackBar(root_lay,"Update Gagal")
            }
    }
}
