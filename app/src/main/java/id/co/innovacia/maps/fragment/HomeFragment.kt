package id.co.innovacia.maps.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import id.co.innovacia.maps.R
import kotlinx.android.synthetic.main.fragment_home.*
import id.co.innovacia.maps.helper.MethodHelper
import id.co.innovacia.maps.model.KontrakanModel
import id.co.innovacia.maps.DetailKontrakan
import android.net.Uri
import android.support.v7.widget.RecyclerView
import com.bumptech.glide.Glide
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.FirebaseDatabase
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import kotlinx.android.synthetic.main.list_home.view.*
import android.Manifest
import android.content.Context
import android.support.v7.app.AlertDialog
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener


class HomeFragment : Fragment() {

    private val logTag = "aim"
    private val mMethodHelper = MethodHelper()

    private var phonePermission = Manifest.permission.CALL_PHONE
    private var telepon = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fetch()

    }

    private fun phoneCall() = runWithPermissions(phonePermission) {
        try {
            val intent = Intent(Intent.ACTION_CALL)
            intent.data = Uri.parse("tel:$telepon")
            startActivity(intent)
        }catch (e: Exception) {
            Log.e("aim", "phone e: $e")
        }
    }

    private fun openWhatsApp() {
        val teleponFormat = if (telepon[0].toString() == "0"){
            telepon.replaceFirst("0","+62")
        } else {
            telepon
        }

        Log.d("aim",teleponFormat)

        val url = "https://api.whatsapp.com/send?phone=$teleponFormat"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }

    private fun getFirebaseKey(koordinat: LatLng, mContext: Context) {
        val builder = AlertDialog.Builder(mContext)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog?.show()

        val rootRef = FirebaseDatabase.getInstance().reference
        val ordersRef = rootRef.child("kontrakan")
            .orderByChild("latitude").equalTo(koordinat.latitude)

        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    dialog?.dismiss()
                    val value = dataSnapshot.value.toString()
                    val firebasekey = value.substring(1,7)

                    val i = Intent(mContext, DetailKontrakan::class.java)
                    i.putExtra("key", firebasekey)
                    startActivity(i)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                dialog?.dismiss()
                Log.d("aim", "loadPost:onCancelled", databaseError.toException())
            }
        }
        ordersRef.addListenerForSingleValueEvent(valueEventListener)
    }

    private fun fetch() {
        // Tampilkan kontrakan yang belum laku
        var query = FirebaseDatabase.getInstance().getReference("kontrakan")
//            .orderByChild("sold")
//            .equalTo("0")

        val options = FirebaseRecyclerOptions.Builder<KontrakanModel>()
            .setQuery(query, KontrakanModel::class.java)
            .setLifecycleOwner(this)
            .build()

        val adapter = object : FirebaseRecyclerAdapter<KontrakanModel, KontrakanHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): KontrakanHolder {
                return KontrakanHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_home, parent, false))
            }

            override fun onBindViewHolder(holder: KontrakanHolder, position: Int, model: KontrakanModel) {
                holder.bindView(model)

                holder.itemView.photo.setOnClickListener {
                    val koordinat = LatLng(model.latitude!!, model.longitude!!)
                    getFirebaseKey(koordinat,holder.itemView.context)
                }

                holder.itemView.telepon.setOnClickListener {
                    if (model.ownerTelepon != null) {
                        telepon = model.ownerTelepon
                        phoneCall()
                    }
                }

                holder.itemView.wa.setOnClickListener {
                    if (model.ownerTelepon != null) {
                        telepon = model.ownerTelepon
                        openWhatsApp()
                    }
                }
            }

            override fun onDataChanged() {
                // Update UI
            }
        }

        recyclerview?.layoutManager = LinearLayoutManager(activity, OrientationHelper.VERTICAL, false)
        recyclerview?.adapter = adapter

    }

    class KontrakanHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(mKontrakanModel: KontrakanModel) {
            val mMethodHelper = MethodHelper()

            itemView.alamat.text = mKontrakanModel.alamat
            itemView.bangunan.text = "${mKontrakanModel.luasBangunan} "
            itemView.kamar.text = mKontrakanModel.kamarTidur.toString()
            itemView.wc.text = mKontrakanModel.kamarMandi.toString()

            val perbulan = mMethodHelper.formatRupiah(mKontrakanModel.perbulan)
            val pertahun = mMethodHelper.formatRupiah(mKontrakanModel.pertahun)

            itemView.bulan.text = "$perbulan / Bulan"
            itemView.tahun.text = "$pertahun / Tahun"

            if (mKontrakanModel.garasi == 0) {
                itemView.garasi.visibility = View.GONE
            } else {
                itemView.garasi.visibility = View.VISIBLE
                itemView.garasi.text = mKontrakanModel.garasi.toString()
            }

            if (mKontrakanModel.foto != null) {
                Glide.with(itemView.context).load(mKontrakanModel.foto[0]).into(itemView.photo)
            }

            if (mKontrakanModel.sold == "0") {
                itemView.sold.visibility = View.GONE
            } else {
                itemView.sold.visibility = View.VISIBLE
            }
        }
    }


}
