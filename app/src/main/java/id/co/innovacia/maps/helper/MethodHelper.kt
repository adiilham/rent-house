package id.co.innovacia.maps.helper

import android.graphics.Color
import android.support.design.widget.Snackbar
import android.view.View
import android.view.animation.AlphaAnimation
import android.widget.TextView
import java.text.NumberFormat
import java.util.*

class MethodHelper {

    companion object {
        // Global variable untuk keseluruhan project
        val animasi = AlphaAnimation(0.1F, 2.0F)

        var userEmail       : String? = null
        var userNama        : String? = null
        var userAlamat      : String? = null
        var userTelepon     : String? = null
        var userState       : Int? = null
    }

    fun snackBar(view: View, pesan: String){
        val mSnackbar = Snackbar.make(view, pesan, Snackbar.LENGTH_LONG)
        val snackbarView = mSnackbar.view
        snackbarView.setBackgroundColor(Color.parseColor("#B2473E"))
        val textView = snackbarView.findViewById(android.support.design.R.id.snackbar_text) as TextView
        textView.setTextColor(Color.parseColor("#FFFFFF"))
        mSnackbar.show()
    }

    fun generatedFirebaseKey(): String {
        // It will generate 6 digit random Number.
        // from 0 to 999999
        val rnd = Random()
        val number = rnd.nextInt(999999)

        // this will convert any number sequence into 6 character.
        return String.format("%06d", number)
    }

    fun formatRupiah(value:Int?): String {
        // Format Rupiah
        val localeID = Locale("in", "ID")
        val rupiah = NumberFormat.getCurrencyInstance(localeID)
        return rupiah.format(value).replace("Rp", "Rp ")
    }
}