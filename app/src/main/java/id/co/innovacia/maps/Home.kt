package id.co.innovacia.maps

import android.annotation.SuppressLint
import android.os.Bundle
import android.os.Handler
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Toast
import id.co.innovacia.maps.fragment.ExploreFragment
import id.co.innovacia.maps.fragment.HomeFragment
import id.co.innovacia.maps.fragment.ProfileFragment
import id.co.innovacia.maps.helper.MethodHelper
import kotlinx.android.synthetic.main.activity_home.*

class Home : AppCompatActivity() {

    val tag = "aim"

    // double click to exit application
    private var doubleBackToExitPressedOnce = false

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->

        when (item.itemId) {
            R.id.navigation_home -> {
                val fragment = HomeFragment()
                showFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_explore-> {
                val fragment = ExploreFragment()
                showFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                val fragment = ProfileFragment()
                showFragment(fragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        if (savedInstanceState == null) {
            val fragmentManager = supportFragmentManager
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.fragment_layout, HomeFragment())
            fragmentTransaction.commit()
        }

        Log.d(tag,"${MethodHelper.userNama},${MethodHelper.userState}")

        nav_view.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            finishAffinity()
            System.exit(0)
            super.onBackPressed()
            return
        }

        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Click back again to exit", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    @SuppressLint("PrivateResource")
    private fun showFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .setCustomAnimations(
                R.anim.abc_grow_fade_in_from_bottom,
                R.anim.abc_shrink_fade_out_from_bottom
            )
            .replace(R.id.fragment_layout, fragment, fragment.javaClass.simpleName)
            .commit()
    }
}
