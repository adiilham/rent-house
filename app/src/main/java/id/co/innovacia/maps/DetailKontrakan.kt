package id.co.innovacia.maps

import android.Manifest
import android.annotation.SuppressLint
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.livinglifetechway.quickpermissions_kotlin.runWithPermissions
import id.co.innovacia.maps.helper.MethodHelper
import id.co.innovacia.maps.model.KontrakanModel
import kotlinx.android.synthetic.main.activity_detail_kontrakan.*
import kotlinx.android.synthetic.main.header.*
import java.util.HashMap

class DetailKontrakan : AppCompatActivity() {

    private var phonePermission = Manifest.permission.CALL_PHONE
    private val mMethodHelper = MethodHelper()
    private var key = ""
    private var teleponString = ""
    private var photo = ""
    private var latitude = 0.0
    private var longitude = 0.0
    private var soldStatus: String? = null

    private var fileFoto = mutableListOf<String>()

    lateinit var mStorageRef: StorageReference

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_kontrakan)
        header_name?.text = "Detail Kontrakan"

        // Update Data Kontrakan
        if (intent.hasExtra("key")) {
            key = intent.getStringExtra("key")
            getDataKontrakan()
        }

        back_link.setOnClickListener {
            onBackPressed()
        }

        review.setOnClickListener {
            val i = Intent(this,Review::class.java)
            i.putExtra("key",key)
            startActivity(i)
        }

        telepon.setOnClickListener {
            phoneCall()
        }

        wa.setOnClickListener {
            openWhatsApp()
        }

        update.setOnClickListener {
            val i = Intent(this, CreateUpdateKontrakan::class.java)
            i.putExtra("update","update")
            i.putExtra("key", key)
            startActivity(i)
        }

        delete.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(this)
            dialogBuilder.setMessage("Lanjutkan menghapus data ?")
                .setCancelable(true)
                .setPositiveButton("Ya") { dialog, id ->
                    deleteData()
                }
                .setNegativeButton("Tidak") { dialog, id ->
                    dialog.cancel()
                }

            val alert = dialogBuilder.create()
            alert.setTitle("Peringatan")
            alert.show()
        }

        validasi.setOnClickListener {
            validasiData()
        }

        sold.setOnClickListener {
            soldData()
        }

        location.setOnClickListener {
            val i = Intent(this, MapsActivity::class.java)
            i.putExtra("latitude",latitude)
            i.putExtra("longitude", longitude)
            startActivity(i)
        }

    }

    private fun getDataKontrakan() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val mDatabase = FirebaseDatabase.getInstance().getReference("kontrakan").child(key)
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                dialog.dismiss()
                if (dataSnapshot.exists()) {
                    val mKontrakanModel = dataSnapshot.getValue(KontrakanModel::class.java)

                    val ownerName = mKontrakanModel?.ownerNama
                    owner_name?.text = ownerName?.capitalize()
                    profile_logo?.text = ownerName?.get(0).toString().capitalize()
                    owner_telepon?.text = mKontrakanModel?.ownerTelepon

                    if (mKontrakanModel?.ownerTelepon != null) {
                        teleponString = mKontrakanModel.ownerTelepon
                    }

                    photo = mKontrakanModel?.foto.toString()
                    alamat?.text = mKontrakanModel?.alamat?.capitalize()
                    tanah?.text = mKontrakanModel?.luasBangunan.toString()
                    kamar?.text = mKontrakanModel?.kamarTidur.toString()
                    wc?.text = mKontrakanModel?.kamarMandi.toString()
                    soldStatus = mKontrakanModel?.sold

                    val perbulan = mKontrakanModel?.perbulan?.let { mMethodHelper.formatRupiah(it) }
                    val pertahun = mKontrakanModel?.pertahun?.let { mMethodHelper.formatRupiah(it) }
                    bulan?.text = perbulan
                    tahun?.text = pertahun

                    if (mKontrakanModel?.garasi == 0) {
                        garasi.visibility = View.GONE
                    } else {
                        garasi.visibility = View.VISIBLE
                        garasi.text = mKontrakanModel?.garasi.toString()
                    }

                    if (mKontrakanModel != null) {
                        latitude = mKontrakanModel.latitude!!
                        longitude = mKontrakanModel.longitude!!
                    }

                    if (mKontrakanModel?.status == 1) {
                        // Jika data sudah tervalidasi maka tulisan peringatan hilang
                        status.visibility = View.GONE
                    } else {
                        // Jika data belum tervalidasi maka akan muncul tulisan peringatan
                        status.visibility = View.VISIBLE
                    }

                    if (mKontrakanModel?.sold == "1") {
                        sold.text = "Rumah ini siap untuk disewakan"
                    } else {
                        sold.text = "Rumah ini telah disewa (sold out)"
                    }

                    if (MethodHelper.userEmail == mKontrakanModel?.ownerEmail){
                        // Jika user sebagai pemilik kontrakan, bisa ubah, sold dan hapus  data kontrakan
                        update.visibility = View.VISIBLE
                        delete.visibility = View.VISIBLE
                        validasi.visibility = View.GONE
                        sold.visibility = View.VISIBLE
                    }else if (MethodHelper.userEmail != mKontrakanModel?.ownerEmail){
                        // Jika user bukan sebagai pemilik kontrakan, hanya bisa view data kontrakan yang belum sold
                        update.visibility = View.GONE
                        delete.visibility = View.GONE
                        validasi.visibility = View.GONE
                        sold.visibility = View.GONE

                        if (MethodHelper.userState == 3) {
                            // Jika admin, bisa hapus, sold dan validasi data kontrakan
                            delete.visibility = View.VISIBLE
                            validasi.visibility = View.VISIBLE
                            sold.visibility = View.VISIBLE
                        }
                    }

                    val photoArray = mKontrakanModel?.foto
                    if (photoArray != null) {
                        for (i in 0 until photoArray.size) {
                            fileFoto.add(photoArray[i])
                        }
                    }
                    if (mKontrakanModel?.foto != null) {
                        Glide.with(this@DetailKontrakan).load(mKontrakanModel.foto[0]).into(photo_lay)
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                dialog.dismiss()
                Log.d("aim", "loadPost:onCancelled", databaseError.toException())
            }
        }
        mDatabase.addListenerForSingleValueEvent(postListener)
    }

    private fun phoneCall() = runWithPermissions(phonePermission) {
        try {
            val intent = Intent(Intent.ACTION_CALL)
            intent.data = Uri.parse("tel:$telepon")
            startActivity(intent)
        }catch (e: Exception) {
            Log.e("aim", "phone e: $e")
        }
    }

    private fun openWhatsApp() {
        val teleponFormat = if (teleponString[0].toString() == "0"){
            teleponString.replaceFirst("0","+62")
        } else {
            teleponString
        }

        Log.d("aim",teleponString)

        val url = "https://api.whatsapp.com/send?phone=$teleponFormat"
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }

    private fun deleteData() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog, null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        // Delete data di firebase database
        val mDatabase = FirebaseDatabase.getInstance().getReference("kontrakan").child(key).removeValue()
        mDatabase.addOnCompleteListener {
            finish()
        }.addOnFailureListener {
            mMethodHelper.snackBar(root_lay,"Proses hapus gagal")
            dialog.dismiss()
        }

        // Delete foto di firebase storage
//        if (fileFoto.isNotEmpty()) {
//            for (i in 0 until fileFoto.size) {
//                mStorageRef = FirebaseStorage.getInstance().reference.child("images").child(fileFoto[i])
//                mStorageRef.delete()
//            }
//        }
    }

    private fun validasiData() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog, null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val mDatabase = FirebaseDatabase.getInstance().getReference("kontrakan").child(key)
            .child("status").setValue(1)
        mDatabase.addOnCompleteListener {
            finish()
        }.addOnFailureListener {
            mMethodHelper.snackBar(root_lay,"Proses validasi gagal")
            dialog.dismiss()
        }
    }

    private fun soldData() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog, null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        if (soldStatus == "1") {
            val mDatabase = FirebaseDatabase.getInstance().getReference("kontrakan").child(key)
                .child("sold").setValue("0")
            mDatabase.addOnCompleteListener {
                finish()
            }.addOnFailureListener {
                mMethodHelper.snackBar(root_lay,"Proses validasi gagal")
                dialog.dismiss()
            }
        } else {
            val mDatabase = FirebaseDatabase.getInstance().getReference("kontrakan").child(key)
                .child("sold").setValue("1")
            mDatabase.addOnCompleteListener {
                finish()
            }.addOnFailureListener {
                mMethodHelper.snackBar(root_lay,"Proses validasi gagal")
                dialog.dismiss()
            }
        }
    }
}
