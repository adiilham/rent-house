package id.co.innovacia.maps.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import id.co.innovacia.maps.model.KontrakanModel
import id.co.innovacia.maps.DetailKontrakan
import id.co.innovacia.maps.R
import kotlinx.android.synthetic.main.fragment_explore.*

class ExploreFragment : Fragment() {

    private var mContext: Context? = null
    private var firebasekey = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_explore, container, false)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment //use SuppoprtMapFragment for using in fragment instead of activity MapFragment = activity SupportMapFragment = fragment
        val cikarang = LatLng(-6.26111,107.15278)


        mapFragment.getMapAsync { mMap ->
            mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            mMap.clear()

            val googlePlex = CameraPosition.builder()
                .target(cikarang)
                .zoom(10F)
                .bearing(0F)
                .tilt(45F)
                .build()
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(googlePlex), 10000, null)

            getDataKontrakan(mMap)

            mMap.setOnMarkerClickListener(object : GoogleMap.OnMarkerClickListener {
                override fun onMarkerClick(marker: Marker): Boolean {
                    Log.d("aim","marker click ${marker.position}")

                    getFirebaseKey(marker.position)
                    return false
                }
            })

        }
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = view.context

        btn_detail?.setOnClickListener {
            val i = Intent(mContext,DetailKontrakan::class.java)
            i.putExtra("key",firebasekey)
            it.context.startActivity(i)
        }
    }

    private fun getDataKontrakan(mMap : GoogleMap) {
        val mDatabase = FirebaseDatabase.getInstance().getReference("kontrakan")
//            .orderByChild("sold")
//            .equalTo("0")
        val postListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    val dataLokasi: MutableList<KontrakanModel> = mutableListOf()
                    dataSnapshot.children.mapNotNullTo(dataLokasi) { it.getValue<KontrakanModel>(KontrakanModel::class.java) }

                    for (i in 0 until dataLokasi.size) {
                        var koordinat = LatLng(dataLokasi[i].latitude!!,dataLokasi[i].longitude!!)
                        Log.d("aim","data : $koordinat")

                        val markerOptions = MarkerOptions()
                        markerOptions.position(koordinat)
//                            .title(dataLokasi[i].ownerNama)
//                            .snippet("aim")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))

                        mMap.addMarker(markerOptions)
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Log.d("aim", "loadPost:onCancelled", databaseError.toException())
            }
        }
        mDatabase.addListenerForSingleValueEvent(postListener)
    }

    private fun getFirebaseKey(koordinat:LatLng) {
        val builder = mContext?.let { AlertDialog.Builder(it) }
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder?.setView(dialogView)
        builder?.setCancelable(false)
        val dialog = builder?.create()
        dialog?.show()

        val rootRef = FirebaseDatabase.getInstance().reference
        val ordersRef = rootRef.child("kontrakan")
            .orderByChild("latitude").equalTo(koordinat.latitude)

        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.exists()) {
                    dialog?.dismiss()
                    val value = dataSnapshot.value.toString()
                    firebasekey = value.substring(1,7)
                    btn_detail?.visibility = View.VISIBLE
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                dialog?.dismiss()
                Log.d("aim", "loadPost:onCancelled", databaseError.toException())
            }
        }
        ordersRef.addListenerForSingleValueEvent(valueEventListener)
    }
}