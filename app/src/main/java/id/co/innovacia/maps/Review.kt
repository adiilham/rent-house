package id.co.innovacia.maps

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.OrientationHelper
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import id.co.innovacia.maps.helper.MethodHelper
import id.co.innovacia.maps.model.ReviewModel
import kotlinx.android.synthetic.main.activity_review.*
import kotlinx.android.synthetic.main.header.*
import kotlinx.android.synthetic.main.list_review.*
import kotlinx.android.synthetic.main.list_review.view.*
import java.lang.reflect.Method
import java.util.HashMap

class Review : AppCompatActivity() {

    private val mMethodHelper = MethodHelper()
    private var reviewKey = mMethodHelper.generatedFirebaseKey()
    private var kontrakanKey: String = ""

    // 0 = create
    // 1 = update
    private var reviewState = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review)
        header_name.text = "Review"

        if (intent.hasExtra("key")) {
            kontrakanKey = intent.getStringExtra("key")
            fetchReview()
            getReviewKey(this)
        }

        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_review.setOnClickListener {
            if (reviewState == 0 && validasiInput()) {
                createReview()
            } else if (reviewState == 1 && validasiInput()) {
                updateReview()
            }
        }

        btn_delete.setOnClickListener {
            deleteDialog()
        }

    }

    private fun getReviewKey(mContext: Context) {
        // get daftar review berdasarkan kontrakan
        val builder = AlertDialog.Builder(mContext)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog?.show()

        val rootRef = FirebaseDatabase.getInstance().reference
        val ordersRef = rootRef.child("review")
            .orderByChild("emailDanKontrakan")
            .equalTo("${MethodHelper.userEmail}<->$kontrakanKey")

        val valueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val emailValidation = dataSnapshot.value.toString()
                Log.d("aim","value : $emailValidation")

                if (dataSnapshot.exists()) {
                    dialog?.dismiss()
                    // jika user sudah pernah review maka update

                    btn_review.visibility = View.VISIBLE
                    btn_delete.visibility = View.VISIBLE
                    val value = dataSnapshot.value.toString()
                    reviewKey = value.substring(1,7)
                    reviewState = 1
                } else {
                    dialog?.dismiss()
                    // jika user belum pernah review maka create

                    btn_review.visibility = View.VISIBLE
                    btn_delete.visibility = View.GONE
                    reviewState = 0
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                dialog?.dismiss()
                Log.d("aim", "loadPost:onCancelled", databaseError.toException())
            }
        }
        ordersRef.addListenerForSingleValueEvent(valueEventListener)
    }

    private fun validasiInput() : Boolean {
        return when {
            review_value.text.length < 5 -> {
                mMethodHelper.snackBar(root_lay,"Minimal 5 karakter")
                false
            }
            review_value.text.length > 100 -> {
                mMethodHelper.snackBar(root_lay,"Maksimal 100 karakter")
                false
            }
            else -> true
        }
    }

    private fun createReview() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val mReviewModel = ReviewModel(
            "${MethodHelper.userEmail}<->$kontrakanKey",
            kontrakanKey,
            MethodHelper.userNama,
            review_value.text.toString()
        )

        FirebaseDatabase.getInstance().reference.child("review").child(reviewKey).setValue(mReviewModel)
            .addOnCompleteListener {
                dialog.dismiss()
                review_value.setText("")
                getReviewKey(this)
            }.addOnFailureListener {
                mMethodHelper.snackBar(root_lay,"Review gagal ditambahkan")
                dialog.dismiss()
            }
    }

    private fun updateReview() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val headers = HashMap<String,String?>()
        headers["emailDanKontrakan"] = MethodHelper.userEmail
        headers["kontrakan"] = kontrakanKey
        headers["komentar"] = review_value.text.toString()
        headers["nama"] = MethodHelper.userNama

        FirebaseDatabase.getInstance().reference.child("review").child(reviewKey).setValue(headers)
            .addOnCompleteListener {
                dialog.dismiss()
                review_value.setText("")
                getReviewKey(this)
            }
            .addOnCanceledListener {
                dialog.dismiss()
                mMethodHelper.snackBar(root_lay,"Review gagal diubah")
            }
    }

    private fun deleteDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        dialogBuilder.setMessage("Lanjutkan menghapus review anda ?")
            .setCancelable(true)
            .setPositiveButton("Ya") { dialog, id ->
                deleteData()
            }
            .setNegativeButton("Tidak") { dialog, id ->
                dialog.cancel()
            }

        val alert = dialogBuilder.create()
        alert.setTitle("Peringatan")
        alert.show()
    }

    private fun deleteData() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog, null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        // Delete data di firebase database
        val mDatabase = FirebaseDatabase.getInstance().getReference("review").child(reviewKey).removeValue()
        mDatabase.addOnCompleteListener {
            getReviewKey(this)
            dialog.dismiss()
        }.addOnFailureListener {
            mMethodHelper.snackBar(root_lay,"Proses hapus gagal")
            dialog.dismiss()
        }
    }

    private fun fetchReview() {
        // Menampilkan review berdasarkan kontrakan
        var query = FirebaseDatabase.getInstance().getReference("review")
            .orderByChild("kontrakan").equalTo(kontrakanKey)

        val options = FirebaseRecyclerOptions.Builder<ReviewModel>()
            .setQuery(query, ReviewModel::class.java)
            .setLifecycleOwner(this)
            .build()

        val adapter = object : FirebaseRecyclerAdapter<ReviewModel, ReviewHolder>(options) {
            override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewHolder {
                return ReviewHolder(
                    LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_review, parent, false))
            }

            override fun onBindViewHolder(holder: ReviewHolder, position: Int, model: ReviewModel) {
                holder.bindView(model)
            }
        }

        recycler_review.layoutManager = LinearLayoutManager(this, OrientationHelper.VERTICAL, false)
        recycler_review.adapter = adapter
    }

    class ReviewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bindView(mReviewModel: ReviewModel) {

            itemView.name_value.text = mReviewModel.nama
            itemView.comment_value.text = mReviewModel.komentar
            itemView.logo_value.text = mReviewModel.nama?.get(0).toString().capitalize()
        }
    }
}
