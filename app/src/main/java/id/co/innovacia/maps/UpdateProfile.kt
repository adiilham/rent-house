package id.co.innovacia.maps

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AlertDialog
import android.util.Log
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import id.co.innovacia.maps.helper.MethodHelper
import id.co.innovacia.maps.model.UserModel
import kotlinx.android.synthetic.main.activity_update_profile.*
import kotlinx.android.synthetic.main.header.*

class UpdateProfile : AppCompatActivity() {

    private val mMethodHelper = MethodHelper()
    lateinit var mAuth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_update_profile)
        header_name.text = "Update Profile"

        mAuth = FirebaseAuth.getInstance()

        back_link.setOnClickListener {
            onBackPressed()
        }

        restoreUserData()

        btn_update?.setOnClickListener {
            btn_update?.startAnimation(MethodHelper.animasi)
            if (validasiSignUp()) {
                update()
            }
        }
    }

    private fun restoreUserData() {
        // Restor data dari class MethodHelper
        signup_user.setText(MethodHelper.userEmail)
        signup_nama.setText(MethodHelper.userNama)
        signup_alamat.setText(MethodHelper.userAlamat)
        signup_telepon.setText(MethodHelper.userTelepon)
    }

    private fun validasiSignUp(): Boolean{
        var cek = 0
        var warning = ""

        if(signup_user.text.isEmpty()){
            warning = "Isi kolom email"; cek++
        }
        if (signup_nama.text.isEmpty()) {
            warning = "Isi kolom nama"; cek++
        }
        if (signup_alamat.text.isEmpty()) {
            warning = "Isi kolom alamat"; cek++
        }
        if (signup_telepon.text.isEmpty()) {
            warning = "isi kolom telepon"; cek++
        }

        if (cek == 0){
            return true
        } else {
            mMethodHelper.snackBar(root_lay,warning)
        }

        return false
    }

    private fun update() {
        val builder = AlertDialog.Builder(this)
        val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
        builder.setView(dialogView)
        builder.setCancelable(false)
        val dialog = builder.create()
        dialog.show()

        val user = mAuth.currentUser

        val mKontrakanModel = UserModel(
            signup_user.text.toString(),
            signup_nama.text.toString(),
            signup_alamat.text.toString(),
            signup_telepon.text.toString(),
            MethodHelper.userState
        )

        FirebaseDatabase.getInstance().reference.child("users").child(user!!.uid).setValue(mKontrakanModel)
            .addOnCompleteListener {
                finish()
            }
            .addOnCanceledListener {
                dialog.dismiss()
                mMethodHelper.snackBar(root_lay,"Proses update gagal")
            }
    }
}
