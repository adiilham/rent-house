package id.co.innovacia.maps.model

data class ReviewModel (
    val emailDanKontrakan: String? = null,
    val kontrakan: String? = null,
    val nama: String? = null,
    val komentar: String? = null
)