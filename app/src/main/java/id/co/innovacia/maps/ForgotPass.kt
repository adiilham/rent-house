package id.co.innovacia.maps

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_forgot_pass.*
import kotlinx.android.synthetic.main.header.*
import com.google.android.gms.tasks.Task
import android.support.annotation.NonNull
import android.support.v7.app.AlertDialog
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.FirebaseAuth
import id.co.innovacia.maps.helper.MethodHelper


class ForgotPass : AppCompatActivity() {

    lateinit var mAuth: FirebaseAuth
    private val mMethodHelper = MethodHelper()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_pass)
        header_name.text = "Pemulihan Akun"

        mAuth = FirebaseAuth.getInstance()

        back_link.setOnClickListener {
            onBackPressed()
        }

        btn_send.setOnClickListener {
            if (email_value.text.isNotEmpty()) {
                val builder = AlertDialog.Builder(this)
                val dialogView = layoutInflater.inflate(R.layout.progress_dialog,null)
                builder.setView(dialogView)
                builder.setCancelable(false)
                val dialog = builder.create()
                dialog.show()

                mAuth.sendPasswordResetEmail(email_value.text.toString()).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        onBackPressed()
                    } else {
                        dialog.dismiss()
                        mMethodHelper.snackBar(root_lay, "Email tidak terkirim")
                    }
                }
            }
        }
    }
}
